#!/usr/bin/env python3

from lsblk import BlockDevice
from lsblk import BlockDevices
import curses

version = "0.10"

class MarcoInfo:
    def __init__(self, titulo): 
        self.posi = 0
        self.titulo = titulo
        self.devices = []
    def prepara(self):
        self.win.clear()
        self.win.box()
        self.win.addstr(0,5,self.titulo)
    def pinta(self, activo):
        fila = 1
        for dev in self.devices:
            attr = curses.A_REVERSE if activo and fila == self.posi+1 else 0
            self.win.addstr(fila,1,dev.get_path(),attr)
            self.win.addstr(fila,13,dev.get_size(),0)
            self.win.addstr(fila,20,dev.get_media_path(),0)
            fila += 1
    def posicion(self, incremento):
        self.posi += incremento
        if self.posi < 0:
            self.posi = 0
        if self.posi >= len(self.devices):
            self.posi = len(self.devices)-1
    def getDevice(self):
        return self.devices[self.posi]

class MarcoGeneral:
    def __init__(self,scr):
        self.scr = scr
        curses.curs_set(0)
        self.marcos = []
        self.marcos.append(MarcoInfo("Desmontados"))
        self.marcos.append(MarcoInfo("Montados"))
        self.activo = self.marcos[0]
        self.dimensiones = ""
    def creacionVentana(self, h, w):
        self.win = curses.newwin(h,w,0,0)
        self.win.keypad(True)
        self.win.clear()
        self.win.box()
        titulo = "GESTION DE MONTAJE DE DISCOS"
        self.win.addstr(0,w//2-len(titulo)//2,titulo)
        wizq = (w-2)//2
        wder = w-2-wizq 
        self.marcos[0].win = curses.newwin(h-2,wizq,1,1)
        self.marcos[1].win = curses.newwin(h-2,wder,1,wizq+1)
    def iniciar(self):
        self.scr.clear()
        h, w = self.scr.getmaxyx()
        nuevaDimensiones = "%dx%d" % (h, w)
        if self.dimensiones != nuevaDimensiones:
            self.creacionVentana(h, w)
            self.dimensiones = nuevaDimensiones
        self.marcos[0].prepara()
        self.marcos[1].prepara()
    def finaliza(self):
        self.win.refresh()
        self.marcos[0].win.refresh()
        self.marcos[1].win.refresh()
    def getch(self):
        return self.win.getch()
    def cambiarActivo(self):
        self.activo = self.marcos[0] if self.activo == self.marcos[1] else self.marcos[1]
    def rellenaDevices(self):
        self.marcos[0].devices = []
        self.marcos[1].devices = []
        for dev in BlockDevices.partitionUsb(True):
            self.marcos[1 if dev.is_mounted() else 0].devices.append(dev)
    def pintaDevices(self):
        for marco in self.marcos:
            marco.pinta(marco == self.activo)
    def posicion(self, incremento):
        self.activo.posicion(incremento)
    def marcoIzq(self):
        return self.marcos[0]
    def marcoDer(self):
        return self.marcos[1]
    def isActivoIzq(self):
        return self.activo == self.marcoIzq()
    def isActivoDer(self):
        return self.activo == self.marcoDer()
    def accion(self):
        if self.isActivoIzq():
            self.activo.getDevice().montar()
        else:
            self.activo.getDevice().desmontar()

def opciones(win, pos, carpetas, activa):
    pos += 3
    for car in carpetas:
        pos -= len(car)
        pos -= 3
    for car in carpetas:
        if int(car[0]) == activa:
            win.addstr(0, pos, car, curses.A_REVERSE)
        else:
            win.addstr(0, pos, car[0], curses.A_BOLD)
            win.addstr(0, pos+1, car[1:], 0)
        pos += len(car)+3

def winAbout(h,w,x,y):
    win = curses.newwin(h,w,x,y)
    win.clear()
    nombre = (
        "X     X  XX  X   X XXXXX  XX  XXX",
        "XX   XX X  X XX  X   X   X  X X  X",
        "X X X X X  X X X X   X   XXXX XXX",
        "X  X  X X  X X  XX   X   X  X X X",
        "X     X  XX  X   X   X   X  X X  X"
    )
    for f in range(0,len(nombre)):
        for c in range(0,len(nombre[f])):
            if nombre[f][c] == 'X':
                win.addstr(3+f,7+c," ",curses.A_REVERSE)
    win.addstr(3,45,"NCurses")
    win.addstr(4,45,"Montar")
    win.addstr(5,45,"USB")
    win.addstr(7,45,version)
    escrito = "Escrito por Faustino Caldera"
    win.addstr(10,(w-len(escrito))//2, escrito)
    git = "https://gitlab.com/montarusb/python"
    win.addstr(11,(w-len(git))//2,git)
    return win

def padAyuda(h,w):
    teclas = (
        "up", "Seleccionar partición anterior",
        "down", "Seleccionar partición posterior",
        "enter", "Montar/desmontar la partición",
        "tab", "Cambiar el panel activo",
        "right", "Seleccinar el panel de montados",
        "left", "Seleccinar el panel de desmontados",
        "q", "Salir montar"
    )
    ma = 0
    for rec in range(0,len(teclas),2):
        ma = max(ma, len(teclas[rec]))
    pad = curses.newpad(max(h,len(teclas)//2),w)
    for rec in range(0,len(teclas),2):
        pad.addstr(rec//2,1+ma-len(teclas[rec]),teclas[rec],curses.A_BOLD)
        pad.addstr(rec//2,ma+3,teclas[rec+1])
    return pad,len(teclas)//2

def help(scr):
    HEIGHT = 16
    WIDTH = 60
    op = ("1:Keys","2:About")
    h, w = scr.getmaxyx()
    hh = (h-HEIGHT)//2
    ww = (w-WIDTH)//2
    win = curses.newwin(HEIGHT,WIDTH,hh,ww)
    win.keypad(True)
    win.addstr(0,3,"montar help", curses.A_BOLD)
    pad, limiteTeclas = padAyuda(h,w)
    k = 0
    opcion = 1
    linea = 0
    while(k != ord('q')):
        win.clear()
        win.box()
        opciones(win, WIDTH-3, op, opcion)
        if opcion == 1:
            textoMas = "--- más ---"
            if linea!= 0:
                win.addstr(1,WIDTH//2-len(textoMas),textoMas)
            if linea < limiteTeclas-HEIGHT+4:
                win.addstr(HEIGHT-2,WIDTH//2-len(textoMas),textoMas)
            win.refresh()
            pad.refresh(linea,0,hh+2,ww+1,hh+HEIGHT-3,ww+WIDTH-2)
        elif opcion == 2:
            win.refresh()
            winA = winAbout(HEIGHT-2,WIDTH-2,hh+1,ww+1)
            winA.refresh()
        k = win.getch()
        if k == ord('1'):
            opcion = 1
            linea = 0
        elif k == ord('2'):
            opcion = 2
        elif k == curses.KEY_LEFT:
            opcion = 1 if opcion == 1 else opcion-1
            linea = 0
        elif k == curses.KEY_RIGHT:
            opcion = len(op) if opcion == len(op) else opcion+1
            linea = 0
        elif k == curses.KEY_UP:
            if linea != 0:
                linea -= 1
        elif k == curses.KEY_DOWN:
            if linea < limiteTeclas-HEIGHT+4:
                linea += 1 
        else:
            k = ord('q')

def drawMenu(scr):
    mg = MarcoGeneral(scr)
    k = 0
    while(k != ord('q')):
        mg.iniciar()
        mg.rellenaDevices()
        mg.pintaDevices()
        mg.finaliza()
        k = mg.getch()
        if k == ord('\t'):
            mg.cambiarActivo()
        elif k == curses.KEY_LEFT:
            mg.activo = mg.marcoIzq()
        elif k == curses.KEY_RIGHT:
            mg.activo = mg.marcoDer()
        elif k == curses.KEY_DOWN:
            mg.posicion(+1)
        elif k == curses.KEY_UP:
            mg.posicion(-1)
        elif k == 10:
            mg.accion()
        elif k == ord('h') or k == ord('H'):
            help(scr)

def main():
    curses.wrapper(drawMenu)

main()