# Montar


Montar es un script en python para agilizar y facilitar el montaje de unidades USB

## Programa necesario instalados
1. lsblk

## Libreria python necesaria
1. curses

* * * 

## Instalación
1. Instalaremos los fichero lsblk.py y montar.py en un directorio cualquiera, por ejemplo, "/root/scripts"

    ```git clone https://gitlab.com/facase-shared/montarUsb.git```

2. Montar un enlace en sbin (solo root)

    ```ln -s /root/scripts/montarUsb/montar.py /usr/local/sbin/montar```

3. Revisar los permisos del enlace creado, si no tiene permiso de ejecución, cambiarlo:

    ```chmod u+x /usr/local/sbin/montar```