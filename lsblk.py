#!/usr/bin/env python3

import subprocess
import json

class BlockDevice:
    def __init__(self, device):
        self.device = device
    def get_path(self):
        return self.device["path"]
    def get_name(self):
        return self.device["kname"]
    def get_children(self):
        children = []
        if self.device.get("children") != None:
            for device in self.device["children"]:
                children.append(BlockDevice(device))
        return children
    def get_media_path_mounted(self):
        return self.device.get("mountpoint")
    def get_label(self):
        return self.device.get("label")
    def is_mounted(self):
        return self.get_media_path_mounted() != None
    def get_size(self):
        return self.device.get("size")
    def get_media_path(self):
        mounted = self.get_media_path_mounted()
        if mounted == None:
            return self.get_media_path_unmounted()
        return mounted
    def get_media_path_unmounted(self):
        label = self.get_label()
        if label != None:
            return "/media/"+label
        else:
            return "/media/"+self.get_name()
    def montar(self):
        if not self.is_mounted():
            path = self.get_media_path_unmounted()
            result = subprocess.getoutput("mkdir -p " + path)
            result = subprocess.getoutput("mount %s %s" % (self.get_path(), path))
    def desmontar(self):
        if self.is_mounted():
            result = subprocess.getoutput("umount " + self.get_media_path_mounted())

class BlockDevices:
    _devices = []
    @staticmethod
    def devices(reset = False):
        if BlockDevices._devices != [] and not reset:
            return BlockDevices._devices
        result = subprocess.getoutput("lsblk -J -O")
        BlockDevices._devices = json.loads(result)["blockdevices"]
        return BlockDevices._devices
    @staticmethod
    def filtro(cierto, falso, reset = False):
        devicesFiltrados = []
        for device in BlockDevices.devices(reset):
            ciertoB = True
            for c in cierto:
                ciertoB = ciertoB and device.get(c) == cierto[c]
            falsoB = False
            for f in falso:
                falsoB = falsoB or device.get(f) == falso[f]
            if ciertoB and not falsoB:
                devicesFiltrados.append(BlockDevice(device))
        return devicesFiltrados
    @staticmethod
    def filtroUsb(reset = False):
        return BlockDevices.filtro({"type":"disk", "tran":"usb"}, {}, reset)
    @staticmethod
    def partitionUsb(reset = False):
        partition = []
        for device in BlockDevices.filtroUsb(reset):
            for part in device.get_children():
                partition.append(part)
        return partition

if __name__ == "__main__":
    #Test
    for device in BlockDevices.filtroUsb():
        for dev in device.get_children():
            print(dev.get_path(), dev.get_name(), dev.is_mounted())
    print("------------------")
    for dev in BlockDevices.partitionUsb():
        print(dev.get_path(), dev.get_name(), dev.is_mounted())